﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMovements : MonoBehaviour
{

    public float moveSpeed = 5.0f;
    public VirtualJoyStick moveJoystickRight;
    private Rigidbody controller;
    private Transform camTransform;

 
    private void Update()   //every frame
    {
        Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // Target.z = transform.position.z;
        Target = moveJoystickRight.InputDirection;
        transform.position = Vector3.MoveTowards(transform.position, Target, moveSpeed * Time.deltaTime); // optimization for different hardware(speed*deltatime)

    }
}
