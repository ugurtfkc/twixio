﻿
using UnityEngine;
using Pathfinding;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]

public class EnemyAI : MonoBehaviour
{

    public Transform target;
    public float updateRate = 2f;
    //current target index
    private int currentWayPoint = 0;

    private Seeker seeker;
    private Rigidbody2D rb;

    public Path path;

    public float speed = 300f;
    public ForceMode2D fMode;   //

    [HideInInspector]
    public bool pathIsEnded = false;

    public float nextWayPointDisctance = 3;

    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            Debug.LogError("Game Over");
            return;
        }
        //start to new path to the target position
        seeker.StartPath(transform.position, target.position, OnPathComplete);
        //
        //
        StartCoroutine (UpdatePath());
    }

    IEnumerator UpdatePath()
    {
        if(target == null)
        {
            //TODO: Insert a player search
          
        }
        seeker.StartPath(transform.position, target.position, OnPathComplete);
        //Suspends the coroutine execution for the given amount of seconds using scaled time.
        yield return new WaitForSeconds(1f/updateRate); 
        StartCoroutine(UpdatePath());
    }

    public void OnPathComplete(Path p)
        {
            Debug.Log("AI is coming to us" + p.error);
            if (!p.error)
            {
                path=p;
                currentWayPoint = 0;
            }

        }

    void FixedUpdate() // physics calculations
    {
        if(target == null)
        {
            return;
        }
        if (path == null)
        {
            return;
        }
        if (currentWayPoint >= path.vectorPath.Count)
        {
            if (pathIsEnded)
                return;
            Debug.Log("Done");
            pathIsEnded = true;
            return;
        }
        pathIsEnded = false;
        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWayPoint] - transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;

        //AI movements
        rb.AddForce(dir, fMode);

        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWayPoint]);
        if(dist< nextWayPointDisctance)
        {
            currentWayPoint++;
            return;
        }
    }


}
