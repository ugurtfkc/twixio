﻿using System.Collections;
using UnityEngine;



public class Movements : MonoBehaviour
{
    public float moveSpeed = 5.0f;
  
    public VirtualJoyStick moveJoystick;
    private Rigidbody controller;
    private Transform camTransform;
    private void Start()
    {
        controller = GetComponent<Rigidbody>();
        controller.maxAngularVelocity = moveSpeed;
      

        camTransform = Camera.main.transform;
    }
    private void Update()   //every frame
    {
         Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
       // Target.z = transform.position.z;
        Target = moveJoystick.InputDirection;
        transform.position = Vector3.MoveTowards(transform.position, Target, moveSpeed * Time.deltaTime); // optimization for different hardware(speed*deltatime)
       
    }
}
